#!/usr/bin/env python3
#!/usr/bin/env python3
import rclpy
from std_msgs.msg import String
from geometry_msgs.msg import Twist


class Test():

    def __init__(self, node):

        self._node = node

        self.publisher_ = self._node.create_publisher(Twist, '/cmd_vel', 10)
        self.cmd = Twist()
        self.turn()

    def turn(self):
        self._node.get_logger().info("TURNING....")
        self.cmd.linear.x = 0.0
        self.cmd.angular.z = 0.5
        self.publisher_.publish(self.cmd)

    def stop(self):
        # self._node.get_logger().info("STOPPING....")
        self.cmd.linear.x = 0.0
        self.cmd.angular.z = 0.0
        self.publisher_.publish(self.cmd)
        # self._node.get_logger().info("STOPPED")

    def on_shutdown_method(self):
        print("on shutdown method called")
        self.stop()


class TestShutdown():
    def on_shutdown_method(self):
        print("on shutdown method called")


def main(args=None):

    rclpy.init(args=args)

    ros_node = rclpy.create_node('test_node')

    test_class = Test(ros_node)
    rclpy.get_default_context().on_shutdown(test_class.on_shutdown_method)

    try:
        rclpy.spin(ros_node)
    except KeyboardInterrupt:
        pass
    finally:
        rclpy.try_shutdown()


if __name__ == '__main__':
    main()
