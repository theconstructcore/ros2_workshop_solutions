import rclpy
# import the ROS2 python dependencies
from rclpy.node import Node
# import the Twist module from geometry_msgs dependencies
from geometry_msgs.msg import Twist

class OnShutDownDemo(Node):

    def __init__(self):
        super().__init__('onshutdwon_demo')
        # create the publisher object
        self.publisher_ = self.create_publisher(Twist, '/robot/cmd_vel', 10)
        self.cmd = Twist()
        self.turn()

    def turn(self):
        self.get_logger().info("TURNING....")
        self.cmd.linear.x = 0.5
        self.cmd.angular.z = 0.5
        self.publisher_.publish(self.cmd)  
    
    def stop(self):
        self.get_logger().info("STOPPING....")
        self.cmd.linear.x = 0.0
        self.cmd.angular.z = 0.0
        self.publisher_.publish(self.cmd)
        self.get_logger().info("STOPPED")

    
    def on_shutdown(self):
        self.stop() 
        
            
def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    demo = OnShutDownDemo()
        
    # pause the program execution, waits for a request to kill the node (ctrl+c)
    rclpy.get_default_context().on_shutdown(demo.on_shutdown)
    try:
        rclpy.spin(demo)
    except KeyboardInterrupt:
        pass
    # Explicity destroy the node
   
    # shutdown the ROS communication
    rclpy.shutdown()

if __name__ == '__main__':
    main()