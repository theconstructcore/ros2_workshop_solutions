# import the Empty module from std_servs service interface
from extra_exercises_custom_interfaces.srv import GoToPosition
from std_srvs.srv import Empty
# import the ROS2 python client libraries
import rclpy
from rclpy.node import Node
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup
from threading import Thread
from rclpy.qos import ReliabilityPolicy
from rclpy.qos import QoSProfile
from geometry_msgs.msg import Point
import time


class GetCirclesAndGo(Node):

    def __init__(self):

        self.send_request_active = False
        # Here we have the class constructor
        self.group1 = MutuallyExclusiveCallbackGroup()
        self.group2 = MutuallyExclusiveCallbackGroup()

        super().__init__('server_client')

        self.client = self.create_client(GoToPosition, '/go_to_position')
        # checks once per second if a service matching the type and name of the client is available.
        while not self.client.wait_for_service(timeout_sec=1.0):
            # if it is not available, a message is displayed
            self.get_logger().info('service /go_to_position not available, waiting again...')

        # Service server
        self.srv = self.create_service(Empty, 
                                        '/search_and_go_to_closest_circle',
                                         self.search_and_go_to_closest_circle_callback,
                                          callback_group=self.group1)
        
        # create a Empty request
        self.req = GoToPosition.Request()
        self.req.go_point.x = 1.0
        self.req.go_point.y = 0.0
        self.req.go_point.z = 1.0
        

        self.subscriber= self.create_subscription(
            Point,
            '/circles',
            self.cicles_callback,
            QoSProfile(depth=10, reliability=ReliabilityPolicy.BEST_EFFORT),
            callback_group=self.group2)


    def cicles_callback(self, msg):
        self.get_logger().warning("Circles Topic="+str(msg))
        # WARNING!!! The Frame of the laser has the Y axis inverted repect to the move_robot frame used.
        # This means that Y values will be inverted. To move the robot to the correct Y location you would need to:
        # Do a TF transform or in this case because its easy enough, we just have to change the sign of the Y values
        self.req.go_point.x = msg.x
        self.req.go_point.y = -1.0*msg.y
        self.req.go_point.z = msg.z
        self.get_logger().warning("Request will be="+str(self.req))

    def send_request(self):
        
        # send the request
        self.get_logger().warning("Waiting for request to Fionish")
        response = self.client.call(self.req)
        self.get_logger().warning("Waiting for request...DONE"+str(response))
        self.send_request_active = False


    def search_and_go_to_closest_circle_callback(self, request, response):
        self.get_logger().warning("Processing Service Server Message...")
        
        # run the send_request() method
        self.send_request_active = True

        self.get_logger().warning("Processing Service Server Message...DONE")

        return response

    def wait_for_sec(self, wait_sec, delta=0.1):
        i = 0
        while i < wait_sec :
             self.get_logger().info("..."+str(i))
             time.sleep(delta)
             i += delta



def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    client_node = GetCirclesAndGo()

    spin_thread = Thread(target=rclpy.spin, args=(client_node,))
    spin_thread.start()

    
    while rclpy.ok():
        if client_node.send_request_active:
            response = client_node.send_request()
            client_node.get_logger().info("RESULT="+str(response))
        else:
            client_node.wait_for_sec(wait_sec=0.1, delta=0.01)

    client_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()