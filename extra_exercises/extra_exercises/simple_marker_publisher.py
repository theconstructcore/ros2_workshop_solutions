import rclpy
# import the ROS2 python libraries
from rclpy.node import Node
# import the Twist module from geometry_msgs interface
from geometry_msgs.msg import Point
from visualization_msgs.msg import Marker
from rclpy.duration import Duration
from rclpy.qos import ReliabilityPolicy
from rclpy.qos import QoSProfile
from rclpy.qos import QoSDurabilityPolicy
from rclpy.qos import QoSLivelinessPolicy
from rclpy.qos import QoSReliabilityPolicy

from rclpy.callback_groups import MutuallyExclusiveCallbackGroup
from rclpy.executors import MultiThreadedExecutor

class CircleMarkerPublisher(Node):

    def __init__(self):

        self.group1 = MutuallyExclusiveCallbackGroup()
        self.group2 = MutuallyExclusiveCallbackGroup()
        # Here we have the class constructor
        # call super() in the constructor in order to initialize the Node object
        # the parameter we pass is the node name
        super().__init__('cicle_marker_publisher_node')


        self.subscriber= self.create_subscription(
            Point,
            '/circles',
            self.cicles_callback,
            QoSProfile(depth=10, reliability=ReliabilityPolicy.BEST_EFFORT),
            callback_group=self.group1)


        # create the publisher object
        # Configuration variables
        qos_profile_publisher = QoSProfile(depth=10)

        # Options  QoSDurabilityPolicy.VOLATILE, QoSDurabilityPolicy.TRANSIENT_LOCAL, 
        qos_profile_publisher.durability = QoSDurabilityPolicy.VOLATILE

        qos_profile_publisher.deadline = Duration(seconds=2)

        # Options QoSLivelinessPolicy.MANUAL_BY_TOPIC, QoSLivelinessPolicy.AUTOMATIC
        qos_profile_publisher.liveliness = QoSLivelinessPolicy.AUTOMATIC

        qos_profile_publisher.liveliness_lease_duration = Duration(seconds=2)

        # Options: QoSReliabilityPolicy.RELIABLE, QoSReliabilityPolicy.BEST_EFFORT
        qos_profile_publisher.reliability = QoSReliabilityPolicy.BEST_EFFORT
        # in this case the publisher will publish on /cmd_vel topic with a queue size of 10 messages.
        # use the Twist module for /cmd_vel topic
        self.publisher_ = self.create_publisher(Marker, 'marker_basic', qos_profile=qos_profile_publisher)
        self.init_marker(index=0,z_val=0.0)
        # define the timer period for 0.5 seconds
        timer_period = 0.1
        # create a timer sending two parameters:
        # - the duration between 2 callbacks (0.5 seeconds)
        # - the timer function (timer_callback)
        self.timer = self.create_timer(timer_period, self.timer_callback, callback_group=self.group2)

    def timer_callback(self):
        # Here we have the callback method
        # Publish the message to the topic
        self.publisher_.publish(self.marker_object)
        # Display the message on the console
        self.get_logger().info('Publishing: "%s"' % self.marker_object)
    
    def cicles_callback(self, msg):
        cicle_x = msg.x
        cicle_y = msg.y
        radius = msg.z
        self.get_logger().info("Publishing: "+str(radius))
        self.update_marker_values(cicle_x, cicle_y, radius)
    
    def update_marker_values(self, in_x, in_y, radius):
        my_point = Point()
        my_point.x = in_x
        my_point.y = in_y
        my_point.z = 0.0
        self.marker_object.pose.position = my_point

        self.marker_object.pose.orientation.x = 0.0
        self.marker_object.pose.orientation.y = 0.0
        self.marker_object.pose.orientation.z = 0.0
        self.marker_object.pose.orientation.w = 1.0
        # We need the diameter
        self.marker_object.scale.x = 1.0 * (2.0* radius)
        self.marker_object.scale.y = 1.0 * (2.0* radius)
        self.marker_object.scale.z = 1.0 * (2.0* radius)

        self.get_logger().info("SCALE: "+str(self.marker_object.scale))

        self.get_logger().warning('UPDATED Markers: "%s"' % self.marker_object)
    
    def init_marker(self,index=0, z_val=0.0):

        self.marker_object = Marker()
        self.marker_object.header.frame_id = "lidar_1_link"
        self.marker_object.header.stamp    = self.get_clock().now().to_msg()
        self.marker_object.ns = ""
        self.marker_object.id = index
        self.marker_object.type = Marker.SPHERE
        self.marker_object.action = Marker.ADD

        my_point = Point()
        my_point.x = 5.0
        my_point.z = 0.0
        self.marker_object.pose.position = my_point

        self.marker_object.pose.orientation.x = 0.0
        self.marker_object.pose.orientation.y = 0.0
        self.marker_object.pose.orientation.z = 0.0
        self.marker_object.pose.orientation.w = 1.0
        self.marker_object.scale.x = 1.0
        self.marker_object.scale.y = 1.0
        self.marker_object.scale.z = 1.0

        self.marker_object.color.r = 1.0
        self.marker_object.color.g = 1.0
        self.marker_object.color.b = 1.0
        # This has to be, otherwise it will be transparent
        self.marker_object.color.a = 1.0
            
        # If we want it for ever, 0, otherwise seconds before desapearing
        """
        builtin_interfaces/Duration lifetime
            int32 sec
            uint32 nanosec

        """
        durantion_obj = Duration().to_msg()
        self.get_logger().info(str(durantion_obj))
        self.marker_object.lifetime = durantion_obj
            
def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    simple_publisher = CircleMarkerPublisher()

    executor = MultiThreadedExecutor(num_threads=2)
    executor.add_node(simple_publisher)
        
    try:
        executor.spin()
    finally:
        executor.shutdown()
        simple_publisher.destroy_node()
            
    
    # shutdown the ROS communication
    rclpy.shutdown()

if __name__ == '__main__':
    main()