import time
import matplotlib.pyplot as plt
import numpy as np

from sklearn.cluster import AgglomerativeClustering
from sklearn.neighbors import kneighbors_graph

from laser_data_sample import LASER_NP_ARRAY_FIXED

from laser_data_sample import angle_min, angle_max, angle_increment


def define_circle(data_array):
    """
    Returns the center and radius of the circle passing the given 3 points.
    In case the 3 points form a line, returns (None, infinity).
    """
    length_array_aux = data_array.shape
    length_array = length_array_aux[0]
    if length_array < 4:
        # We cant calculate it or we can but its three points and its too low
        cx = None
        cy = None
        radius = None
    else:    
        p1 =  data_array[0]
        p2 = data_array[int(length_array/2)]
        p3 = data_array[length_array-1]

        temp = p2[0] * p2[0] + p2[1] * p2[1]
        bc = (p1[0] * p1[0] + p1[1] * p1[1] - temp) / 2
        cd = (temp - p3[0] * p3[0] - p3[1] * p3[1]) / 2
        det = (p1[0] - p2[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p2[1])

        if abs(det) < 1.0e-6:
            # For the case of infinite radius value            
            cx = 0
            cy = 0
            radius = None
        else:
            # Center of circle
            cx = (bc*(p2[1] - p3[1]) - cd*(p1[1] - p2[1])) / det
            cy = ((p1[0] - p2[0]) * cd - (p2[0] - p3[0]) * bc) / det

            radius = np.sqrt((cx - p1[0])**2 + (cy - p1[1])**2)
    
    return [cx, cy, radius]


def check_if_circle(center_point, radius, testing_points_array, delta=0.009, rating_limit=0.8):

    if radius is not None:
        length_testing_points = testing_points_array.shape[0]
        acumulated_success = 0.0

        for test_point in testing_points_array:

            dist = np.linalg.norm(center_point-test_point)
            difference = abs(radius - dist)
            is_circle = difference <= delta
            acumulated_success += int(is_circle)

        circle_rating = float(acumulated_success) / float(length_testing_points)


        result= circle_rating > rating_limit
    else:
        result = False
        circle_rating = 0.0

    return result, circle_rating


def main(plot_debug = False):
    

    angle_range = angle_max - angle_min
    # Create angle range
    angle_range = np.arange(angle_min, angle_max, angle_increment)
    angle_range_fix = angle_range.reshape(1,-1)
    t_sin = np.sin(angle_range_fix)
    t_cos = np.cos(angle_range_fix)


    x = np.multiply(LASER_NP_ARRAY_FIXED, t_cos)
    y = np.multiply(LASER_NP_ARRAY_FIXED, t_sin)

    X = np.concatenate((x, y))
    X = X.T

    #############################

    # Create a graph capturing local connectivity. Larger number of neighbors
    # will give more homogeneous clusters to the cost of computation
    # time. A very large number of neighbors gives more evenly distributed
    # cluster sizes, but may not impose the local manifold structure of
    # the data
    n_clusters = 15
    knn_graph = kneighbors_graph(X, n_clusters, include_self=False)


    connectivity = knn_graph
    plt.figure(figsize=(10, 4))
    index = 0 
    linkage = 'single'
    plt.subplot(1, 1, index + 1)
    model = AgglomerativeClustering(linkage=linkage,
                                    connectivity=connectivity,
                                    n_clusters=n_clusters)
    t0 = time.time()
    model.fit(X)

    unique_model_labels = np.unique(model.labels_)
    num_labels = unique_model_labels.shape[0]

    # objects_array = np.full((num_labels, 1), None)
    zip_objects_array = zip(range(num_labels), num_labels * [None])
    objects_array = {}

    index_label = 0
    for element in X:
        
        label = str(model.labels_[index_label])
        

        # We place each element in different arrays, depending on their Label
        if label not in objects_array:
            objects_array[label] = [element]
        else:
            objects_array[label].append(element)

        index_label += 1



    elapsed_time = time.time() - t0

    if plot_debug:
        plt.scatter(X[:, 0], X[:, 1], c=model.labels_,
                    cmap=plt.cm.nipy_spectral)
        plt.title('linkage=%s\n(time %.2fs)' % (linkage, elapsed_time),
                    fontdict=dict(verticalalignment='top'))
        plt.axis('equal')
        plt.axis('off')

        plt.subplots_adjust(bottom=0, top=.83, wspace=0,
                            left=0, right=1)
        plt.suptitle('n_cluster=%i, connectivity=%r' %
                        (n_clusters, connectivity is not None), size=17)

        plt.show()

    # Plot all the different clusters by labels




    cicle_dict = {}
    cicle_dict_result = {}

    # PPaint Different labeled points
    for key, value in objects_array.items():
        value_NP_ARRAY = np.array(value)

        ## Detect circle

        cicle_array_data = define_circle(data_array=value_NP_ARRAY)
        cicle_dict[key] = cicle_array_data
        cicle_center_np = np.array([cicle_array_data[0],cicle_array_data[1]])

        result, circle_rating = check_if_circle(center_point=cicle_center_np, 
                        radius=cicle_array_data[2], 
                        testing_points_array=value_NP_ARRAY,
                        delta=0.009,
                        rating_limit=0.7)
        
        cicle_dict_result[key] = [result, circle_rating]

        # Plot
        if plot_debug:
            plt.scatter(value_NP_ARRAY[:, 0], value_NP_ARRAY[:, 1])
            plt.title("Label="+key+",CIRCLE="+str(result)+",circle_rating="+str(circle_rating))
            plt.axis('equal')
            plt.axis('off')

            plt.subplots_adjust(bottom=0, top=.83, wspace=0,
                                left=0, right=1)
            
            plt.show()

    print("CIRCLE DICT="+str(cicle_dict))
    print("CICLE RESULT="+str(cicle_dict_result))



if __name__ == "__main__":
    main(plot_debug = True)
