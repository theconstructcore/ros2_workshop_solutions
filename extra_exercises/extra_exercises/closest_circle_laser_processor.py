import rclpy
# import the ROS2 python libraries
from rclpy.node import Node
# import the LaserScan module from sensor_msgs interface
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Point
# import Quality of Service library, to set the correct profile and reliability in order to read sensor data.
from rclpy.qos import ReliabilityPolicy
from rclpy.qos import QoSProfile
from rclpy.qos import QoSDurabilityPolicy
from rclpy.qos import QoSLivelinessPolicy
from rclpy.qos import QoSReliabilityPolicy
from rclpy.duration import Duration

from extra_exercises.laser_data_processing import LaserDataProcess
from extra_exercises.simple_marker_publisher import CircleMarkerPublisher

import random

from rclpy.callback_groups import MutuallyExclusiveCallbackGroup
from rclpy.executors import MultiThreadedExecutor

import math

class LaserScanCircleDetector(Node):

    def __init__(self):
        
        self.laser_Data_obj = LaserDataProcess(plot_debug=False)
        
        # Here we have the class constructor
        # call super() in the constructor in order to initialize the Node object
        # the parameter we pass is the node name
        super().__init__('laserScancicledetector_node_closest')
        # create the subscriber object
        # in this case the subscriptor will be subsribed on /scan topic with a queue size of 10 messages.
        # use the LaserScan module for /scan topic
        # send the received info to the listener_callback method.
        self.subscriber= self.create_subscription(
            LaserScan,
            '/scan',
            self.listener_callback,
            QoSProfile(depth=10, reliability=ReliabilityPolicy.BEST_EFFORT))

        # We create the detection publisher
        qos_profile_publisher = QoSProfile(depth=10)
        qos_profile_publisher.durability = QoSDurabilityPolicy.VOLATILE
        qos_profile_publisher.deadline = Duration(seconds=2)
        qos_profile_publisher.liveliness = QoSLivelinessPolicy.AUTOMATIC
        qos_profile_publisher.liveliness_lease_duration = Duration(seconds=2)
        qos_profile_publisher.reliability = QoSReliabilityPolicy.BEST_EFFORT
        self.publisher_ = self.create_publisher(Point, '/circles', qos_profile=qos_profile_publisher)
        

    def listener_callback(self, msg, IMPOSSIBLE_RADIUS=2.0):
        # print the log info in the terminal
        
        #self.get_logger().info('I receive: "%s"' % str(msg))
        cicle_dict, cicle_dict_result  = self.detect_circle(msg)
        self.get_logger().warning("cicle_dict="+str(cicle_dict))
        self.get_logger().warning("cicle_dict_result="+str(cicle_dict_result))

        # If the circle is beyond 30 meters we dondt detect it
        distance_from_circle_max = 5.0
        closest_circle_data = None
        closest_circle_name = "NONAME"

        for key, value in cicle_dict_result.items():
            
            is_circle = value[0]
            cicle_quality = value[1]
            if  is_circle:
                cicle_data = cicle_dict[key]                
                center_x = cicle_data[0]
                center_y = cicle_data[1]
                radius = cicle_data[2]
                if radius < IMPOSSIBLE_RADIUS:
                    # Its considered to be false positive if bigger than IMPOSSIBLE RADIUS
                    # We add this data to the circles_subset
                    distance_from_circle = math.sqrt(pow(center_x,2) + pow(center_y,2))
                    if distance_from_circle < distance_from_circle_max:
                        closest_circle_data = cicle_data
                        distance_from_circle_max = distance_from_circle
                        closest_circle_name = key

        # We should have the closest circle
        p_obj = Point()
        p_obj.x = closest_circle_data[0]
        p_obj.y = closest_circle_data[1]
        p_obj.z = closest_circle_data[2]  

        self.get_logger().warning("Name=="+str(closest_circle_name)+", Dist="+str(distance_from_circle)+", CLOSEST CIRCLE PUBLISH="+str(p_obj))

        self.publisher_.publish(p_obj)        
                
        
        

    def detect_circle(self,laser_msg):
        
        return self.laser_Data_obj.find_circles(laser_msg)
        
        
            
def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    simple_subscriber = LaserScanCircleDetector()
    marker_publisher = CircleMarkerPublisher()
    # pause the program execution, waits for a request to kill the node (ctrl+c)
    executor = MultiThreadedExecutor(num_threads=3)
    executor.add_node(simple_subscriber)
    executor.add_node(marker_publisher)
        
    try:
        executor.spin()
    finally:
        executor.shutdown()
        simple_subscriber.destroy_node()
        marker_publisher.destroy_node()
            
    
    # shutdown the ROS communication
    rclpy.shutdown()


if __name__ == '__main__':
    main()