# import the SetBool module from std_servs service interface
from extra_exercises_custom_interfaces.srv import GoToPosition
# import the Twist module from geometry_msgs messages interface
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry
# import the ROS2 python client libraries
import rclpy
from rclpy.node import Node
import time
from rclpy.qos import ReliabilityPolicy, QoSProfile
from extra_exercises.quat_to_euler import euler_from_quaternion
import math
import copy
import numpy as np

class MoveRobotToPoint(Node):

    def __init__(self, cmd_vel_topic="/cmd_vel", odom_topic='/odom'):
        # Here we have the class constructor
        self._cmd_vel_topic = cmd_vel_topic
        self._odom_topic = odom_topic
        # call the class constructor to initialize the node as service_moving
        super().__init__('move_robot_to_point')
        # create the service server object
        # defines the type, name and callback function
        self.srv = self.create_service(GoToPosition, '/go_to_position', self.go_to_position_callback)
       
        # create the publisher object
        # in this case the publisher will publish on /cmd_vel topic with a queue size of 10 messages.
        # use the Twist module
        self.publisher_ = self.create_publisher(Twist, self._cmd_vel_topic, 10)
        # create a Twist message
        self.cmd = Twist()

        self.init_variables()

        self.subscriber= self.create_subscription(
            Odometry,
            self._odom_topic,
            self.odom_callback,
            QoSProfile(depth=10, reliability=ReliabilityPolicy.BEST_EFFORT))
    
    def init_variables(self):
        self.reached_goal = True
        self.reached_angle_goal = True
        self.reached_distance_goal = True
        
        self.x_pos = 0.0
        self.y_pos = 0.0
        self.yaw = 0.0

        self.x_pos_service_call = 0.0
        self.y_pos_service_call = 0.0
        self.yaw_service_call = 0.0

        self.turn_angle = 0.0
        self.move_distance = 0.0
        self.move_distnace_security = 0.0
        self.turn_speed = 0.1
        self.move_speed = 0.1

        self.error = 0.01

    def go_to_position_callback(self, request, response):
        self.get_logger().warning("Processing Service Server Message...")
        
        # We retrieve the X, y and z values
        x_val = request.go_point.x
        y_val = request.go_point.y
        self.move_distnace_security = request.go_point.z

        # We calculate the angle and distance that the robot has to turn and move to
        # We use last known location to calculat the deltas to the new location
        self.x_pos_service_call = copy.deepcopy(self.x_pos)
        self.y_pos_service_call = copy.deepcopy(self.y_pos)
        self.yaw_service_call = copy.deepcopy(self.yaw)

        # in robot reference
        delta_x = x_val
        delta_y = y_val

        self.turn_angle = math.atan2(delta_y,delta_x)
        self.move_distance = math.sqrt(pow(delta_x,2) + pow(delta_y,2))

        response.turn_angle = self.turn_angle
        response.move_distance = self.move_distance

        self.reached_goal = False
        self.reached_angle_goal = False
        self.reached_distance_goal = False

        self.get_logger().warning("Processing Service Server Message...DONE")

        return response
    
    def wait_for_sec(self, wait_sec, delta=0.1):
        i = 0
        while i < wait_sec :
             self.get_logger().info("..."+str(i))
             time.sleep(delta)
             i += delta
    
    def odom_callback(self, msg):        
        
        self.x_pos = msg.pose.pose.position.x
        self.y_pos = msg.pose.pose.position.y
        quaternion_obj = msg.pose.pose.orientation
        roll, pitch, self.yaw = euler_from_quaternion(quaternion_obj)

        if not self.reached_goal:
            twist_msg = Twist()

            # Zero angle special case
            if self.turn_angle == 0.0:
                self.reached_angle_goal = True
                twist_msg.linear.x = 0.0
                twist_msg.angular.z = 0.0
                self.publisher_.publish(twist_msg)

            if not self.reached_angle_goal:
                turn_sign = np.sign([self.turn_angle])[0]

                twist_msg.linear.x = 0.0
                twist_msg.angular.z = self.turn_speed * turn_sign
                self.publisher_.publish(twist_msg)
                
                # This is to solve the issue that the odometry sensor goes from 0 to pi
                # and from 0 to -pi. If we turn more than pi, it jumps to the negative values
                # And that messes up with the calculations.
                if turn_sign  >= 0.0:
                    # We are turning counter clockwise
                    if self.yaw < 0.0:
                        # We have made a complete turn and things wont work
                        self.get_logger().error("RECALCULATING NEGATIVE YAW")
                        self.yaw = 2*math.pi + self.yaw
                else:
                    # We are turning clockwise
                    if self.yaw > 0.0:
                        # We will have issues
                        self.get_logger().error("RECALCULATING POSITIVE YAW")
                        self.yaw = -2*math.pi + self.yaw

                moved_angle = self.yaw - self.yaw_service_call

                is_similar = np.isclose([moved_angle], [self.turn_angle], atol=self.error)
                self.get_logger().error("moved_angle="+str(moved_angle))
                self.get_logger().error("self.turn_angle"+str(self.turn_angle))

                if is_similar.all():
                    self.reached_angle_goal = True
                    twist_msg.linear.x = 0.0
                    twist_msg.angular.z = 0.0
                    self.publisher_.publish(twist_msg)
                    self.get_logger().warning("Reached Angle Goal")
                else:
                    self.get_logger().error("NOT reached angle goal yet = "+str(is_similar.all()))
                    
            else:
                if not self.reached_distance_goal:
                    # We have to reach now the distance goal
                    twist_msg.linear.x = self.move_speed
                    twist_msg.angular.z = 0.0
                    self.publisher_.publish(twist_msg)

                    delta_x = self.x_pos - self.x_pos_service_call
                    delta_y = self.y_pos - self.y_pos_service_call
                    distance_travelled = math.sqrt(pow(delta_x,2) + pow(delta_y,2))

                    if distance_travelled > self.move_distance:
                        self.reached_distance_goal = True
                        twist_msg.linear.x = 0.0
                        twist_msg.angular.z = 0.0
                        self.publisher_.publish(twist_msg)
                        self.get_logger().warning("Reached Distance Goal")
                    else:
                        self.get_logger().error("NOT Reached Distance Goal")
                        self.get_logger().error("distance_travelled="+str(distance_travelled))
                        self.get_logger().error("self.move_distance"+str(self.move_distance))
                else:
                    self.reached_goal = True
                    self.get_logger().error("GOAL ACHIEVED!!")
        
        else:
            #self.get_logger().info("["+str(self.x_pos)+","+str(self.y_pos)+","+str(self.yaw)+"]")
            pass

        



def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    move_robot_to_point_service_node = MoveRobotToPoint()
    # pause the program execution, waits for a request to kill the node (ctrl+c)
    rclpy.spin(move_robot_to_point_service_node)
    move_robot_to_point_service_node.destroy_node()
    # shutdown the ROS communication
    rclpy.shutdown()


if __name__ == '__main__':
    main()