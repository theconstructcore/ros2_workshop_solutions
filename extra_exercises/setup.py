from setuptools import setup

package_name = 'extra_exercises'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='tgrip',
    maintainer_email='duckfrost@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'simple_laser_processor = extra_exercises.simple_laser_processor:main',
            'simple_marker_publisher = extra_exercises.simple_marker_publisher:main',
            'move_robot_to_point = extra_exercises.move_robot_to_point:main',
            'move_robot_to_point_multithreading = extra_exercises.move_robot_to_point_multithreading:main',            
            'search_circles_and_go = extra_exercises.search_circles_and_go:main',
            'closest_circle_laser_processor = extra_exercises.closest_circle_laser_processor:main'
            
        ],
    },
)
