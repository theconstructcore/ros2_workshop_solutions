import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.substitutions import LaunchConfiguration

from launch_ros.actions import Node
import xacro
import random


def generate_launch_description():

    move_robot_node = Node(
        package='cartographer_slam',
        executable='moving_robot_exe',
        output='screen',
        parameters=[{'use_sim_time': True}],
        name='moving_robot_node')

    # create and return launch description object
    return LaunchDescription(
        [
            move_robot_node
        ]
    )
